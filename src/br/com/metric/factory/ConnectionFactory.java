package br.com.metric.factory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.Properties;

public class ConnectionFactory {

	private static Properties properties = new Properties();
	
	//	private static String JDBC_URL = "jdbc:postgresql://localhost:5435/metric";
	private static String JDBC_URL = "jdbc:mysql://br-cdbr-azure-south-b.cloudapp.net:3306/metric";
	private static String JDBC_USER = "b7b2bb0d55eb75";
	private static String JDBC_PASS = "4043cc00";
	private static String DRIVER_CLASS  = "com.mysql.jdbc.Driver";

	private static class Params {
		public static final String P_CFG_ATIVO = "P_CFG_ATIVO";
		public static final String P_JDBC_URL = "P_JDBC_URL";
		public static final String P_JDBC_USER = "P_JDBC_USER";
		public static final String P_JDBC_PASS = "P_JDBC_PASS";
		public static final String P_DRIVE_CLASS = "P_DRIVER_CLASS";
	}
	
	public static String getCleanPath() {
	    ClassLoader classLoader = ConnectionFactory.class.getClassLoader();
	    File classpathRoot = new File(classLoader.getResource("").getPath());

	    return classpathRoot.getPath();
	}
	
	static {

		try {
//			loadParameters((System.getProperty("user.dir") + "\\metric.cfg"));
			loadParameters(getCleanPath() +"\\metric.cfg");
//			loadParameters("C:\\Metric\metric.cfg");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			boolean cfgAtivo = getStringParameter(Params.P_CFG_ATIVO).equals("TRUE");
			
			System.out.println("CFG_ATIVO="+cfgAtivo);
			
			if (cfgAtivo) {
				JDBC_URL = getStringParameter(Params.P_JDBC_URL);
				JDBC_USER = getStringParameter(Params.P_JDBC_USER);
				JDBC_PASS = getStringParameter(Params.P_JDBC_PASS);
				DRIVER_CLASS = getStringParameter(Params.P_DRIVE_CLASS);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadParameters (String fileName) throws Exception {
		
		InputStream in = null;
		
		try {
			in = new FileInputStream(fileName);
			properties.load(in);
			
			for (Map.Entry<Object, Object> entry : properties.entrySet())
				saveParameter(entry.getKey(), entry.getValue());
			
		} catch (Exception e) {
			throw(e);
		} finally {
			if (in != null)
				try {in.close();} catch (Exception e) {}
		}
	}
	
	public static void saveParameter (Object key, Object value) {
		System.getProperties().put(key, value);
	}
	
	public static String getStringParameter(String key) throws Exception {
		String value = properties.getProperty(key);
		
		if (value == null)
			throw new Exception(key);

		return value;
	}
	
	public static Integer getIntegerParameter (String key) throws Exception {
		String value = getStringParameter(key);
		
		if (value == null)
			throw new Exception(key);
		
		return Integer.valueOf(value);
	}
	
	public static Connection open () throws Exception {
		Connection conn = null;
//		Class.forName("org.postgresql.Driver").newInstance();
		Class.forName(DRIVER_CLASS).newInstance();
		conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
		conn.setAutoCommit(false);
		return conn;
	}
	
	public static void close (Object... objs) {
		if (objs == null || objs.length == 0)
			return;
		
		for (int i = 0; i < objs.length; i++) {
			
			if (objs[i] == null)
				continue;
			
			if (objs[i] instanceof Connection)
				try{((Connection) objs[i]).close();} catch (Exception e) {}
			else if (objs[i] instanceof ResultSet)
				try{((ResultSet) objs[i]).close();} catch (Exception e) {}
			else if (objs[i] instanceof PreparedStatement)
				try{((PreparedStatement) objs[i]).close();} catch (Exception e) {}
			else if (objs[i] instanceof CallableStatement)
				try{((CallableStatement) objs[i]).close();} catch (Exception e) {}
		}
	}
}