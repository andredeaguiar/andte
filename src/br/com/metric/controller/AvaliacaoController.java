package br.com.metric.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.metric.dao.AvaliacaoDao;
import br.com.metric.model.Avaliacao;
import br.com.metric.model.Caracteristicas;
import br.com.metric.model.Sistema;
import br.com.metric.utils.Messages;

public class AvaliacaoController {

	private ArrayList<Sistema> sistemas = new ArrayList<>();
	private ArrayList<Caracteristicas> listaCaracteristicas = new ArrayList<>();
	private Sistema sistema = new Sistema();
	
	public ArrayList<Caracteristicas> getListaCaracteristicas() {
		return listaCaracteristicas;
	}

	public void setListaCaracteristicas(ArrayList<Caracteristicas> listaCaracteristicas) {
		this.listaCaracteristicas = listaCaracteristicas;
	}

	public ArrayList<Sistema> getSistemas() {
		return sistemas;
	}

	public void setSistemas(ArrayList<Sistema> sistemas) {
		this.sistemas = sistemas;
	}
	
	public Caracteristicas getPeso (int idCaracteristica) {
		for (Caracteristicas caracteristicas : listaCaracteristicas)
			if (caracteristicas.getIdCaracteristicas() == idCaracteristica)
				return caracteristicas;
		
		return null;
	}
	
	public String comparar() {
		
		if (sistemas == null || sistemas.isEmpty() || sistemas.size() <= 1) {
			Messages.exibeMensagemErro("Por favor selecione ao menos 2 sistemas para comparar...");
			return "";
		}
		
		List<Avaliacao> listaAvaliacao;
		double soma =0;
		for (Sistema sistema : sistemas) {
			listaAvaliacao = new AvaliacaoDao().getAvaliacao(sistema.getId());
			for (Avaliacao avaliacao : listaAvaliacao) {
				avaliacao.setDescSistema(sistema.getNome());
				avaliacao.setSomaNotas(avaliacao.getSomaNotas() * getPeso(avaliacao.getIdCaracteristicas()).getPesoCaracteristicas());
				soma += avaliacao.getSomaNotas();
			}
			sistema.setMediaNotas(soma / 6);
			soma =0;
			sistema.setListaAvaliacao((ArrayList<Avaliacao>) listaAvaliacao);
		}
		getMelhorSistema();
//		this.sistema.setMelhorSistema(true);
		return "resultado?faces-redirect=true";
	}
	
	public void getMelhorSistema () {
		
		for (Sistema sis : sistemas) {
			
			if (sistema == null) {
				this.sistema = sis;
				continue;
			}
			
			if (sistema != null && sistema.getMediaNotas() < sis.getMediaNotas()) {
				for (Sistema sis2 : sistemas)
					sis2.setMelhorSistema(false);
				
				sis.setMelhorSistema(true);
				this.sistema = sis;
			}
		}
		
	}
	

}
