package br.com.metric.controller;

import java.util.ArrayList;

import javax.faces.component.html.HtmlInputHidden;

import org.richfaces.event.DropEvent;

import br.com.metric.dao.CaracteristicasDao;
import br.com.metric.dao.SistemaDao;
import br.com.metric.model.Caracteristicas;
import br.com.metric.model.Sistema;
import br.com.metric.utils.Messages;

public class SistemaController {

	private ArrayList<Sistema> sistemas = new ArrayList<>();
	private ArrayList<Sistema> listaTarget = new ArrayList<>();
	private ArrayList<Caracteristicas> listaCaracteristicas = new ArrayList<>();
	
	private Sistema sistema = new Sistema();
	
	@SuppressWarnings("unchecked")
	public SistemaController() {
		sistemas = (ArrayList<Sistema>) new SistemaDao().listar();
		reload();
	}
	
	public ArrayList<Caracteristicas> getListaCaracteristicas() {
		return listaCaracteristicas;
	}

	public void setListaCaracteristicas(ArrayList<Caracteristicas> listaCaracteristicas) {
		this.listaCaracteristicas = listaCaracteristicas;
	}

	public Sistema getSistema () {
		return sistema;
	}
	
	public void setSistema (Sistema sistema) {
		this.sistema = sistema;
	}
	
	public ArrayList<Sistema> getSistemas () {
		return sistemas;
	}
	
	public void setSistemas (ArrayList<Sistema> sistemas) {
		this.sistemas = sistemas;
	}
	
	public void salvarSistema () {
		
		System.out.println("Salvar");
		
//		boolean hasMsg = false;
		
//		for (Caracteristicas car : listaCaracteristicas) {	
//			Double peso = car.getPesoCaracteristicas();
//			if (peso == null || peso < 0) {
//				if (!hasMsg) {
//					Messages.exibeMensagemErro("Por favor informe a nota das caracteristicas >= 0...");
//					hasMsg = true;
//				}
//			}
//			
//			for (SubCaracteristicas sub : car.getSubCaracteristicas())
//				if (sub.getPesoSubCaracteristica() == null || sub.getPesoSubCaracteristica() < 0) {
//					if (!hasMsg) {
//						Messages.exibeMensagemErro("Por favor informe a nota das sub caracteristicas >= 0...");
//						hasMsg = true;
//					}
//				}
//		}

//		if (hasMsg)
//			return;
		
		if (sistema.getNome() == null || sistema.getNome().isEmpty()) {
			Messages.exibeMensagemErro("Por favor informe o nome do sistema para salvar...");
			return;
		}
		
		if (new SistemaDao().salvar(sistema, listaCaracteristicas)) {
			reload();
			Messages.exibeMensagemErro("Salvo com sucesso!");
			return; // msg de salvo
		} else { 
			Messages.exibeMensagemErro("Erro ao Salvar Sistema!");
			return; // msg de erro
		}
	}
	
	@SuppressWarnings("unchecked")
	public void reload() {
		System.out.println("Reload Data...");
		sistema = new Sistema();
		listaCaracteristicas = (ArrayList<Caracteristicas>) new CaracteristicasDao().listar();
	}
	
	public void processDrop (DropEvent drop) {
		System.out.println(sistema.getNome());	
		Sistema sistema = (Sistema)drop.getDragValue();
		if (drop.getDropValue().equals("add")) {
			listaTarget.add(sistema);
			sistemas.remove(sistema);
		} else {
			listaTarget.remove(sistema);
			sistemas.add(sistema);
		}
	}

//	private boolean validatePesos () {
//		
//		if (listaCaracteristicas == null || listaCaracteristicas.isEmpty())
//			return false;
//		
//		double soma = 0.0d;
//		double peso = 0.0d;
//		boolean hasMsg = false;
//		
//		for (Caracteristicas car : listaCaracteristicas) {	
//			peso = car.getPesoCaracteristicas();
//			
//			for (SubCaracteristicas sub : car.getSubCaracteristicas())
//				soma += sub.getPesoSubCaracteristica();
//			
//			if (peso != soma) {
//				Messages.exibeMensagemErro("O peso da Caracteristica: " + car.getDescCaracteristicas() + " est�o distribuidos de forma incorreta. Verifique.");
//				hasMsg = true;
//			}
//		}
//		
//		return !hasMsg;
//	}
	
	public void validaSistemas () {
		if (listaTarget == null || listaTarget.isEmpty() || listaTarget.size() <= 1) {
			Messages.exibeMensagemErro("Por favor selecione ao menos 2 sistemas para comparar...");
		}
	}
	
	@SuppressWarnings("unchecked")
	public String limpaSistemas() {
		listaTarget.clear();
		sistemas = (ArrayList<Sistema>) new SistemaDao().listar();
		return "index?faces-redirect=true";
	}
	
	public ArrayList<Sistema> getListaTarget() {
		return listaTarget;
	}

	public void setListaTarget(ArrayList<Sistema> listaTarget) {
		this.listaTarget = listaTarget;
	}
	
	@SuppressWarnings("unchecked")
	public HtmlInputHidden getInitView(){
		sistemas = (ArrayList<Sistema>) new SistemaDao().listar();
		return null;
	}
	public void setInitView(HtmlInputHidden htmlInputHidden){
		
	}

}