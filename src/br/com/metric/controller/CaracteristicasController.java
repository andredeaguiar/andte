package br.com.metric.controller;

import java.util.ArrayList;

import org.richfaces.event.DropEvent;

import br.com.metric.model.Caracteristicas;
import br.com.metric.model.PerguntaChave;
import br.com.metric.model.SubCaracteristicas;

public class CaracteristicasController {

	public CaracteristicasController() {
		init();
	}

	public void init() {
		ArrayList<SubCaracteristicas> subCaracteristicas = new ArrayList<SubCaracteristicas>();
		SubCaracteristicas subCaracteristica = new SubCaracteristicas();
		subCaracteristica.setDescSubCaracterisca("Capacidade para substituir");
		subCaracteristica.setIdCaracteristica(1);
		subCaracteristica.setIdSubCaracteristica(1);
		ArrayList<PerguntaChave> perguntasChave = new ArrayList<PerguntaChave>();
		PerguntaChave perguntaChave = new PerguntaChave();
		perguntaChave.setDescPergunta("Prop�e-se a fazer o que � apropriado?");
		perguntaChave.setIdSubCaracteristica(1);
		perguntasChave.add(perguntaChave);
		perguntasChave.add(perguntaChave);
		perguntasChave.add(perguntaChave);
		subCaracteristica.setPerguntasChave(perguntasChave);
		subCaracteristicas.add(subCaracteristica);
		Caracteristicas caracteristica = new Caracteristicas();
		caracteristica.setDescCaracteristicas("Portabilidade");
		caracteristica.setSubCaracteristicas(subCaracteristicas);
		caracteristica.setIdCaracteristicas(1);
		caracteristicas.add(caracteristica);
	}

	private ArrayList<Caracteristicas> caracteristicas = new ArrayList<Caracteristicas>();
	private ArrayList<Caracteristicas> caracteristicasTarget = new ArrayList<Caracteristicas>();

	public ArrayList<Caracteristicas> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(ArrayList<Caracteristicas> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public void processDrop(DropEvent event) {

		Object drop = event.getDragValue();
		ArrayList<Caracteristicas> aux;
		String dropValue = (String) event.getDropValue();

		if (dropValue.equals("add")) {
			aux = caracteristicasTarget;
		} else {
			aux = caracteristicas;
		}
		
		if (drop instanceof Caracteristicas) {
			aux.add((Caracteristicas) drop);
		} else if (drop instanceof SubCaracteristicas) {
			
			Caracteristicas car = getCatFromList(aux, (SubCaracteristicas) drop);
			
			if (car == null) {
				car = getCatFromList(caracteristicasTarget, (SubCaracteristicas) drop);
				if (car == null) 
					car = getCatFromList(caracteristicas, (SubCaracteristicas) drop);
			}
			
			if (car.getSubCaracteristicas() == null)
				car.setSubCaracteristicas(new ArrayList<SubCaracteristicas>());
			
			car.getSubCaracteristicas().add((SubCaracteristicas) drop);
			
			aux.add(car);
		}
		
		if (dropValue.equals("add")) {
			aux = caracteristicas;
		} else {
			aux = caracteristicasTarget;
		}
		
		if (drop instanceof Caracteristicas) {
			aux.remove(drop);
		} else if (drop instanceof SubCaracteristicas) {
			removeSub(aux,(SubCaracteristicas) drop);
		} else if (drop instanceof PerguntaChave) {
			SubCaracteristicas sub = getSubFromPergunta(aux, (PerguntaChave) drop);
			sub.getPerguntasChave().remove((PerguntaChave) drop);
			if (sub.getPerguntasChave() == null || sub.getPerguntasChave().isEmpty()) {
				removeSub(aux, sub);
			}
		}
	}

	private void removeSub(ArrayList<Caracteristicas> list, SubCaracteristicas sub) {
		Caracteristicas car = getCatFromList(list, sub);

		Caracteristicas aux = list.get(list.indexOf(car));

		aux.getSubCaracteristicas().remove(sub);

		if (aux.getSubCaracteristicas() == null || aux.getSubCaracteristicas().isEmpty())
			list.remove(aux);
	}

	private Caracteristicas getCatFromList(ArrayList<Caracteristicas> list, SubCaracteristicas sub) {
		if (list == null)
			return null;
		
		Caracteristicas caracteristica = null;
		for (int i = 0; i < list.size(); i++) {
			caracteristica = list.get(i);
			if (caracteristica.getIdCaracteristicas() == sub.getIdCaracteristica())
				return caracteristica;
		}

		return caracteristica;
	}

	private SubCaracteristicas getSubFromPergunta(ArrayList<Caracteristicas> list,PerguntaChave pergunta) {

		SubCaracteristicas sub = null;

		for (int i = 0; i < list.size(); i++) {
			Caracteristicas caracteristica = list.get(i);
			for (int j = 0; j < caracteristica.getSubCaracteristicas().size(); j++) {
				SubCaracteristicas subAux = caracteristica.getSubCaracteristicas().get(j);
				if (subAux.getIdSubCaracteristica() == pergunta.getIdSubCaracteristica())
					return subAux;
			}
		}
		return sub;
	}
	
	private Caracteristicas getCatFromPergunta (ArrayList<Caracteristicas> list, PerguntaChave perguntaChave) {
		return getCatFromList(list, getSubFromPergunta(list, perguntaChave));
	}

	public ArrayList<Caracteristicas> getCaracteristicasTarget() {
		return caracteristicasTarget;
	}

	public void setCaracteristicasTarget(ArrayList<Caracteristicas> caracteristicasTarget) {
		this.caracteristicasTarget = caracteristicasTarget;
	}
}
