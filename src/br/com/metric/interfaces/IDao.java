package br.com.metric.interfaces;

import java.util.List;

public interface IDao {
	public boolean remover(int id);
	public boolean alterar(Object obj);
	public boolean salvar(Object obj);
	public List<?> listar ();
	public List<?> listar (Object obj);
}
