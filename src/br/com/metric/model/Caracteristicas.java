package br.com.metric.model;

import java.util.ArrayList;

public class Caracteristicas {

	private int idCaracteristicas;
	private String descCaracteristicas;
	private Double pesoCaracteristicas;
	private Double notaCaracteristica;

	private ArrayList<SubCaracteristicas> subCaracteristicas;

	public int getIdCaracteristicas() {
		return idCaracteristicas;
	}

	public void setIdCaracteristicas(int idCaracteristicas) {
		this.idCaracteristicas = idCaracteristicas;
	}

	public String getDescCaracteristicas() {
		return descCaracteristicas;
	}

	public void setDescCaracteristicas(String descCaracteristicas) {
		this.descCaracteristicas = descCaracteristicas;
	}

	public Double getPesoCaracteristicas() {
		return pesoCaracteristicas;
	}

	public void setPesoCaracteristicas(Double pesoCaracteristicas) {
		this.pesoCaracteristicas = pesoCaracteristicas;
	}

	public ArrayList<SubCaracteristicas> getSubCaracteristicas() {
		return subCaracteristicas;
	}

	public void setSubCaracteristicas(ArrayList<SubCaracteristicas> subCaracteristicas) {
		this.subCaracteristicas = subCaracteristicas;
	}

	public Double getNotaCaracteristica() {
		return notaCaracteristica;
	}

	public void setNotaCaracteristica(Double notaCaracteristica) {
		this.notaCaracteristica = notaCaracteristica;
	}
}