package br.com.metric.model;

import java.util.ArrayList;

public class Sistema {

	private int id;
	private String nome;
	private ArrayList<Avaliacao> listaAvaliacao;
	private double mediaNotas;
	private boolean melhorSistema;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Avaliacao> getListaAvaliacao() {
		return listaAvaliacao;
	}

	public void setListaAvaliacao(ArrayList<Avaliacao> listaAvaliacao) {
		this.listaAvaliacao = listaAvaliacao;
	}

	public double getMediaNotas() {
		return mediaNotas;
	}

	public void setMediaNotas(double mediaNotas) {
		this.mediaNotas = mediaNotas;
	}

	public boolean isMelhorSistema() {
		return melhorSistema;
	}

	public void setMelhorSistema(boolean melhorSistema) {
		this.melhorSistema = melhorSistema;
	}

}
