package br.com.metric.model;

import java.util.ArrayList;

public class SubCaracteristicas {

	private int idSubCaracteristica;
	private String descSubCaracterisca;
	private Double pesoSubCaracteristica;
	private int idCaracteristica;
	private Double notaSubcaracteristica;

	private ArrayList<PerguntaChave> perguntasChave;

	public int getIdSubCaracteristica() {
		return idSubCaracteristica;
	}

	public void setIdSubCaracteristica(int idSubCaracteristica) {
		this.idSubCaracteristica = idSubCaracteristica;
	}

	public String getDescSubCaracterisca() {
		return descSubCaracterisca;
	}

	public void setDescSubCaracterisca(String descSubCaracterisca) {
		this.descSubCaracterisca = descSubCaracterisca;
	}

	public Double getPesoSubCaracteristica() {
		return pesoSubCaracteristica;
	}

	public void setPesoSubCaracteristica(Double pesoSubCaracteristica) {
		this.pesoSubCaracteristica = pesoSubCaracteristica;
	}

	public int getIdCaracteristica() {
		return idCaracteristica;
	}

	public void setIdCaracteristica(int idCaracteristica) {
		this.idCaracteristica = idCaracteristica;
	}

	public ArrayList<PerguntaChave> getPerguntasChave() {
		return perguntasChave;
	}

	public void setPerguntasChave(ArrayList<PerguntaChave> perguntasChave) {
		this.perguntasChave = perguntasChave;
	}

	public Double getNotaSubcaracteristica() {
		return notaSubcaracteristica;
	}

	public void setNotaSubcaracteristica(Double notaSubcaracteristica) {
		this.notaSubcaracteristica = notaSubcaracteristica;
	}

}