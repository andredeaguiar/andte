package br.com.metric.model;

public class Avaliacao {

	private int idSistema;
	private int idCaracteristicas;
	private String descCaracteristicas;
	private Double somaNotas;
	private String descSistema;
	private double mediaAvaliacao;
	
	public int getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(int idSistema) {
		this.idSistema = idSistema;
	}

	public int getIdCaracteristicas() {
		return idCaracteristicas;
	}

	public void setIdCaracteristicas(int idCaracteristicas) {
		this.idCaracteristicas = idCaracteristicas;
	}

	public Double getSomaNotas() {
		return somaNotas;
	}

	public void setSomaNotas(Double somaNotas) {
		this.somaNotas = somaNotas;
	}

	public String getDescCaracteristicas() {
		return descCaracteristicas;
	}

	public void setDescCaracteristicas(String descCaracteristicas) {
		this.descCaracteristicas = descCaracteristicas;
	}

	public String getDescSistema() {
		return descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public double getMediaAvaliacao() {
		return mediaAvaliacao;
	}

	public void setMediaAvaliacao(double mediaAvaliacao) {
		this.mediaAvaliacao = mediaAvaliacao;
	}

}
