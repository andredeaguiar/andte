package br.com.metric.model;

public class PerguntaChave {

	private int idPergunta;
	private String descPergunta;
	private int idSubCaracteristica;

	public String getDescPergunta() {
		return descPergunta;
	}

	public void setDescPergunta(String descPergunta) {
		this.descPergunta = descPergunta;
	}

	public int getIdSubCaracteristica() {
		return idSubCaracteristica;
	}

	public void setIdSubCaracteristica(int idSubCaracteristica) {
		this.idSubCaracteristica = idSubCaracteristica;
	}

	public int getIdPergunta() {
		return idPergunta;
	}

	public void setIdPergunta(int idPergunta) {
		this.idPergunta = idPergunta;
	}

}