package br.com.metric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.metric.factory.ConnectionFactory;
import br.com.metric.model.Avaliacao;

public class AvaliacaoDao {

	public List<Avaliacao> getAvaliacao (int idSistema) {
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<Avaliacao> listaAvaliacao = new ArrayList<>();
		
		try {
		
			conn = ConnectionFactory.open();
			pstm = conn.prepareStatement("select id_sistema, a.id_caracteristicas,desc_caracteristicas, sum(nota) as sum_notas from avaliacao a "
					+ "inner join caracteristicas c on c.id_caracteristicas = a.id_caracteristicas "
					+ "where id_sistema = ? "
					+ "group by id_sistema, a.id_caracteristicas,desc_caracteristicas order by id_caracteristicas ;");
			
			pstm.setInt(1, idSistema);
			rs = pstm.executeQuery();
			
			while (rs.next()) {
				Avaliacao avaliacao = new Avaliacao();
				avaliacao.setIdSistema(rs.getInt("id_sistema"));
				avaliacao.setIdCaracteristicas(rs.getInt("id_caracteristicas"));
				avaliacao.setSomaNotas(rs.getDouble("sum_notas"));
				avaliacao.setDescCaracteristicas(rs.getString("desc_caracteristicas"));
				listaAvaliacao.add(avaliacao);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs, pstm, conn);
		}
		return listaAvaliacao;
	}
	
}
