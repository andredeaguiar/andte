package br.com.metric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.metric.factory.ConnectionFactory;
import br.com.metric.interfaces.IDao;
import br.com.metric.model.Caracteristicas;
import br.com.metric.model.SubCaracteristicas;

public class CaracteristicasDao implements IDao {

	@Override
	public boolean remover(int id) {
		return false;
	}

	@Override
	public boolean alterar(Object obj) {
		return false;
	}

	@Override
	public boolean salvar(Object obj) {
		return false;
	}

	@Override
	public List<?> listar() {
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<Caracteristicas> lista = new ArrayList<>();
		
		try {
			
			conn = ConnectionFactory.open();
			pstm = conn.prepareStatement("SELECT * FROM Caracteristicas");
			rs = pstm.executeQuery();
			
			Caracteristicas caracteristicas;
			while (rs.next()) {
				caracteristicas = new Caracteristicas();
				caracteristicas.setIdCaracteristicas(rs.getInt("id_Caracteristicas"));
				caracteristicas.setDescCaracteristicas(rs.getString("desc_Caracteristicas"));
				caracteristicas.setPesoCaracteristicas((double)rs.getInt("peso_Caracteristicas"));
				caracteristicas.setSubCaracteristicas((ArrayList<SubCaracteristicas>) new SubCaracteristicasDao().listar(conn, caracteristicas.getIdCaracteristicas()));
				
				lista.add(caracteristicas);
			}
			
		} catch (Exception e) {
			
		} finally {
			ConnectionFactory.close(rs, pstm, conn);
		}
		
		return lista;
	}

	@Override
	public List<?> listar(Object obj) {
		return null;
	}
}