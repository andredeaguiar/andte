package br.com.metric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.metric.factory.ConnectionFactory;
import br.com.metric.model.SubCaracteristicas;

public class SubCaracteristicasDao {

	public List<?> listar (Connection conn, int idCaracteristicas) {
		
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		List<SubCaracteristicas> lista = new ArrayList<>();
		try {
			
			pstm = conn.prepareStatement("SELECT * FROM SubCaracteristicas WHERE id_Caracteristicas = ?");
			pstm.setInt(1, idCaracteristicas);
			rs = pstm.executeQuery();
			
			SubCaracteristicas sub;
			while (rs.next()) {
				sub = new SubCaracteristicas();
				sub.setIdSubCaracteristica(rs.getInt("id_subcaracteristica"));
				sub.setIdCaracteristica(idCaracteristicas);
				sub.setDescSubCaracterisca(rs.getString("desc_SubCaracteristica"));
				sub.setPesoSubCaracteristica((double) rs.getInt("peso_SubCaracteristica"));
				lista.add(sub);
			}
			
		} catch (Exception e) {
			
		} finally {
			ConnectionFactory.close(rs, pstm);
		}
		return lista;
	}
	
}
