
/*TO DO LIST: 
Alterar tabela cad_param, adicionando a coluna IP, implmentando a FUNCTION GetCurrentIP via trigger 
Definir nomenclatura de tabelas
Entender como funcionam inserts para multiplas tabelas com chave composta e cria procedure que permita isso
Criar procedures que permitam alteração em tabelas com chaves compostas(multi-column) 
Criar procedure de CASCADE DELETE que não viole as PK ou FK
Criar views do sistema(falar com Gu)
Validar funcionamento 

TO READ:

http://stackoverflow.com/questions/868620/sql-script-to-alter-all-foreign-keys-to-add-on-delete-cascade
https://www.mssqltips.com/sqlservertip/2365/sql-server-foreign-key-update-and-delete-rules/
http://stackoverflow.com/questions/253849/cannot-truncate-table-because-it-is-being-referenced-by-a-foreign-key-constraint
https://technet.microsoft.com/en-us/library/aa902684(v=sql.80).aspx
https://sqlandme.com/2011/08/08/sql-server-how-to-cascade-updates-and-deletes-to-related-tables/
http://support.sas.com/documentation/cdl/en/proc/61895/HTML/default/viewer.htm#a002473671.htm
http://stackoverflow.com/questions/4888277/add-default-value-of-datetime-field-in-sql-server-to-a-timestamp
http://stackoverflow.com/questions/92082/add-a-column-with-a-default-value-to-an-existing-table-in-sql-server
http://www.codeproject.com/Tips/333634/How-to-get-Client-IP-Address-in-SQL-Server
http://www.pucrs.br/famat/viali/tic_literatura/dissertacoes/Gladcheff_Ana_Paula.pdf -- exemplo de sistema de qualidade de software, baixar ASAP
http://www.w3schools.com/sql/sql_foreignkey.asp
https://msdn.microsoft.com/en-us/library/ms189049.aspx
http://www.codeproject.com/Tips/333634/How-to-get-Client-IP-Address-in-SQL-Server
http://pt.stackoverflow.com/questions/301/devo-escrever-meu-programa-em-ingl%C3%AAs-ou-portugu%C3%AAs
http://pt.stackoverflow.com/questions/31646/padr%C3%A3o-de-nomenclatura-no-c%C3%B3digo-para-o-c
http://pt.stackoverflow.com/questions/301/devo-escrever-meu-programa-em-ingl%C3%AAs-ou-portugu%C3%AAs
https://dzone.com/articles/20-database-design-best
http://comunidade.itlab.com.br/eve/forums/a/tpc/f/273606921/m/8631093852
http://imasters.com.br/artigo/24756/banco-de-dados/verdades-e-mitos-sobre-joins/




/* Cria Function para carregar o IP de quem disparou a function que irá abastecer a coluna IP da tabela cad_param, via trigger %nome%. Fonte: http://www.codeproject.com/Tips/333634/How-to-get-Client-IP-Address-in-SQL-Server */
/*CREATE FUNCTION [dbo].[GetCurrentIP] ()
RETURNS varchar(255)
AS
BEGIN
    DECLARE @IP_Address varchar(255);
 
    SELECT @IP_Address = client_net_address
    FROM sys.dm_exec_connections
    WHERE Session_id = @@SPID;
 
    Return @IP_Address;
END
*/


/*
SELECT es.program_name, es.host_name,
es.login_name, COUNT(ec.session_id) AS [connection_count]
FROM sys.dm_exec_sessions AS es
INNER JOIN sys.dm_exec_connections AS ec ON es.session_id = ec.session_id
GROUP BY es.program_name, es.host_name, es.login_name


select HOST_NAME ()
select @@ServerName

*/